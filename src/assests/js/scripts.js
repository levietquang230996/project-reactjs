import 'jquery/dist/jquery.min';
import 'popper.js/dist/popper.min';
import 'bootstrap/dist/js/bootstrap.min';
import './sidebar-menu';
import 'datatables/media/js/jquery.dataTables.min';
import '../plugins/slick/slick.min';
import './app-script';
