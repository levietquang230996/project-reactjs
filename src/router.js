import React from 'react';
import PageHome from "./pages/home/pages/PageHome";
import PageAdmin from "./pages/admin/pages/PageAdmin";
import PageLogin from "./pages/sign/pages/PageLogin";
import PageUsers from "./pages/admin/pages/User/PageUsers";
import PagePortfolio from "./pages/admin/pages/Portfolio/PagePortfolio";
import PageWhySna from "./pages/home/pages/PageWhySna";
import MainLayout from "./pages/home/MainLayout";
import PageProcess from "./pages/home/pages/PageProcess";

export const list_router_pages = [
   {
      path: '/login',
      exact: false,
      main: () => <PageLogin/>
   },
   {
      path: '/',
      exact: false,
      main: () => <MainLayout/>,
      routes: [
         {
            path: '',
            exact: true,
            main: () => <PageHome/>
         },
         {
            path: 'home',
            exact: false,
            main: () => <PageHome/>
         },
         {
            path: 'why-sna',
            exact: false,
            main: () => <PageWhySna/>
         },
         {
            path: 'process',
            exact: false,
            main: () => <PageProcess/>
         },
      ]
   },
   {
      path: '/admin',
      exact: false,
      main: () => <PageAdmin/>,
      routes: [
         {
            path: '/',
            exact: true,
            main: () => <PageUsers/>
         },
         {
            path: '/users',
            exact: false,
            main: () => <PageUsers/>
         },
         {
            path: '/portfolio',
            exact: false,
            main: () => <PagePortfolio/>
         },
      ]
   },
];
