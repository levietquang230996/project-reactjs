import {takeLatest, call, put} from 'redux-saga/effects';
import {
   SEND_DECREMENT_NUMBER,
   SEND_INCREMENT_NUMBER,
   SUCCESS_DECREMENT_NUMBER,
   SUCCESS_INCREMENT_NUMBER
} from "../reducer/actions";

let index = -1;
const Number = ['One', 'Two', 'Three', 'Four', 'Five'];

export function* watcherSaga() {
   yield takeLatest(SEND_DECREMENT_NUMBER, decrementWorkerSaga);
   yield takeLatest(SEND_INCREMENT_NUMBER, incrementWorkerSaga);
}

export function* decrementWorkerSaga() {
   try {
      const num = yield call(fetchPreviousNum);
      yield put({type: SUCCESS_DECREMENT_NUMBER, num})
   } catch (e) {
      console.log(e)
   }
}

export function* incrementWorkerSaga() {
   try {
      const num = yield call(fetchNextNum);
      yield put({type: SUCCESS_INCREMENT_NUMBER, num})
   } catch (e) {
      console.log(e)
   }
}

const fetchPreviousNum = async () => {
   --index;
   if (index < 0) index = Number.length - 1
   await sleep(2000);
   return Number[index];
};

const fetchNextNum = async () => {
   ++index;
   if (index > Number.length - 1) index = 0;
   await sleep(2000);
   return Number[index];
}

const sleep = (ms) => (new Promise(resolve => setTimeout(resolve, ms)));
