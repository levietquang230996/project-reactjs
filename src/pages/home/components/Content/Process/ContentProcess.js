import React, {Component} from 'react';
import BGVideo from "../WhySna/BGVideo/BGVideo";
import RopeCurtain from "../Home/RopeCurtain/RopeCurtain";
import OurProcess from "./OurProcess/OurProcess";


class ContentProcess extends Component {

   render() {
      return (
         <React.Fragment>

            <BGVideo/>

            <RopeCurtain/>

            <OurProcess/>

            <RopeCurtain/>

         </React.Fragment>
      )
   }
}

export default (ContentProcess);
