import React, {Component} from 'react';
import './index.css';

class SectionMap extends Component {

   componentDidMount() {
      const path = document.querySelector('.line');
      const length = path.getTotalLength();

      path.style.strokeDasharray = length;
      path.style.strokeDashoffset = length;

// we can set the speed as well
      path.style.animationDuration = "5s";
   }

   render() {
      return (
         <React.Fragment>
            <div className="container_map position-relative m-auto p-3">
               <div id="chartdiv" className="w-100 h-100"/>
               <div className="bg-transparent position-absolute w-100 h-100 top-0 left-0">
                  <svg className="dashed-line position-absolute top-0 left-0" width="1024px" height="500px"
                       xmlns="http://www.w3.org/2000/svg">
                     <g stroke="none" strokeWidth="2" fill="none" fillRule="evenodd">
                        <g stroke="#ffbb00">
                           <g>
                              <path d="M176,500 C300,300 700,250 790,260"
                                    className='line' />
                           </g>
                           <g>
                              <path d="M520,500 C600,350 700,250 790,290"
                                    className='line' strokeDasharray="5,5"/>
                           </g>
                           <g>
                              <path d="M855,500 C880,300 800,250 799,280"
                                    className='line' strokeDasharray="5,5"/>
                           </g>
                        </g>
                     </g>

                     <g stroke="black" strokeWidth="2" fill="#ffbb00">
                        <circle cx="790" cy="260" r="3"/>
                        <circle cx="790" cy="290" r="3"/>
                        <circle cx="799" cy="280" r="3"/>
                     </g>
                  </svg>
                  <div className="bg-white position-absolute w-25 h-25 bottom-0 left-0"/>
               </div>
            </div>
            <div className="content_map row m-auto no-gutters">
               <div className="col-12 col-md-4">a</div>
               <div className="col-12 col-md-4">b</div>
               <div className="col-12 col-md-4">c</div>
            </div>
         </React.Fragment>
      )
   }
}

export default SectionMap;