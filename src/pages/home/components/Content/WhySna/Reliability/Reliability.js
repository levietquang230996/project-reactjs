import React, {Component} from 'react';
import './index.css';
import icon1 from './../../../../../../assests/images/whysna/icon1.png';
import icon2 from './../../../../../../assests/images/whysna/icon8.png';
import icon3 from './../../../../../../assests/images/whysna/icon9.png';
import $ from "jquery";

class Reliability extends Component {

   componentDidMount() {
      $(window).on("scroll", function () {
         if (2100 < $(this).scrollTop() && $(this).scrollTop() < 2300) {
            $('.bg_reliability.animated').addClass('rollInRight');
         }
      });
   }

   render() {
      return (
         <React.Fragment>
            <section className="reliability">
               <div className="container">
                  <p className="font-RoL custom_title text-center">Reliability <span className="color_sna">Goal</span>
                  </p>
                  <p className="font-RoR reliability_content text-center m-auto">
                     Before taking on any project we first analyze requirements documentation, define use
                     cases and then identify and select proper technologies to establish to establish a
                     budget proposal with resource plan and high-level timeline.
                  </p>
                  <div className="row no-gutters bg_reliability py-4 position-relative animated">
                     <div className="col-12 col-lg-4 reliability_item text-center p-3">
                        <img src={icon1} alt="dedicated" className="my-5"/>
                        <p className="font-RoL reliability_item_title mb-4">+10 Yr. Experience</p>
                        <p className="font-RoR reliability_item_content">
                           Each of our managers is +10 years experienced and embodied in the software
                           development process. Having experience in numerous areas including mobile,
                           finance, retails and smart factory, we suggest the best way to achieve your goal.
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 reliability_item text-center p-3">
                        <img src={icon2} alt="dedicated" className="my-5"/>
                        <p className="font-RoL reliability_item_title mb-4">Strong Fundamental</p>
                        <p className="font-RoR reliability_item_content">
                           SNA is business for 6 years and have experience in various projects including
                           mobile, web, video analytics.
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 reliability_item text-center p-3">
                        <img src={icon3} alt="dedicated" className="my-5"/>
                        <p className="font-RoL reliability_item_title mb-4">Partnership</p>
                        <p className="font-RoR reliability_item_content">
                           We have partnership with IBM, Oracle, Microsoft, Amazon for years and
                           have a good relationship in between.
                        </p>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default (Reliability);
