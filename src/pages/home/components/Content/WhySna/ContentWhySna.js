import React, {Component} from 'react';
import BGVideo from "./BGVideo/BGVideo";
import ChooseUs from "./ChooseUs/ChooseUs";
import OwnTeam from "./OwnTeam/OwnTeam";
import Security from "./Security/Security";
import RopeCurtain from "../Home/RopeCurtain/RopeCurtain";
import Reliability from "./Reliability/Reliability";


class ContentWhySna extends Component {

   render() {
      return (
         <React.Fragment>

            <BGVideo/>

            <ChooseUs/>

            <OwnTeam/>

            <Security/>

            <RopeCurtain/>

            <Reliability/>

         </React.Fragment>
      )
   }
}

export default (ContentWhySna);
