import React, {Component} from 'react';
import video from '../../../../../../assests/videos/hero.mp4';
import './../../Home/BGVideo/BGVideo.css';

class BGVideo extends Component {

   componentDidMount() {
   }

   render() {
      const src_IMG = 'https://static1.cbrimages.com/wordpress/wp-content/uploads/2019/10/Demon-Slayer-Similar-Anime-featured-image.jpg';
      const src_VIDEO = video;

      return (
         <React.Fragment>
            {/* Panel */}
            <section className="video">
               <div className="position-relative container-video">
                  <video poster={src_IMG} autoPlay loop muted>
                     <source src={src_VIDEO} type="video/mp4"/>
                  </video>
                  <div className={"content-video position-absolute w-100 text-white"}>
                     <div className="container">
                        <p className="font-RoM custom_width_title font-weight-bold m-0">
                           TRANSFER YOUR IDEA INTO REALITY</p>
                        <div className="font-RoR pt-3 custom_width_content">
                           <p>Welcome to your software development team!</p>
                           <p>We build apps, webs and software with a passion</p>
                        </div>
                        <button
                           className="font-RoM btn bg_color_sna text-white mt-2 mt-md-4 mt-lg-5 py-1
                           px-3 py-md-2 px-md-4 py-lg-3 px-lg-5">
                           START A PROJECT
                        </button>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default BGVideo;
