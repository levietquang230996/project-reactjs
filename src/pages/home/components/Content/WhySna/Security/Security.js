import React, {Component} from 'react';
import icon1 from "../../../../../../assests/images/whysna/icon4.png";
import icon2 from "../../../../../../assests/images/whysna/icon5.png";
import icon3 from "../../../../../../assests/images/whysna/icon6.png";
import './index.css';
import $ from "jquery";

class Security extends Component {

   componentDidMount() {
      $(window).on("scroll", function () {
         if (1500 < $(this).scrollTop() && $(this).scrollTop() < 1600) {
            $('.security .delay-1').addClass('bounceIn');
            $('.security .delay-2').addClass('bounceIn');
            $('.security .delay-3').addClass('bounceIn');
         }
      });
   }

   render() {
      return (
         <React.Fragment>
            <section className="security">
               <div className="container">
                  <p className="font-RoL custom_title security_title text-white text-center">
                     <span className="color_sna">Security </span>First Policy</p>
                  <p className="font-RoR security_content text-center text-white m-auto">
                     With the Security-First policy, we consider your data and information vary seriously.
                     All project members are required to sign NDA before any project starts. We are well prepared
                     on both logical and physical security to protect your intellectual properties.
                  </p>
                  <div className="row no-gutters">
                     <div className="col-12 col-lg-4 security_item text-center p-4">
                        <img src={icon1} alt="dedicated" className="my-5 animated delay-3"/>
                        <p className="font-RoL security_item_title color_sna mb-4">Employee NDA</p>
                        <p className="font-RoR security_item_content text-white">
                           To protect your intellectual properties, all our employees are signed on NDA before
                           involving your project.</p>
                     </div>
                     <div className="col-12 col-lg-4 security_item text-center p-4">
                        <img src={icon2} alt="dedicated" className="my-5 animated delay-2"/>
                        <p className="font-RoL security_item_title color_sna mb-4">Data Security</p>
                        <p className="font-RoR security_item_content text-white">
                           Any data transfer of your project is only allowed in certain circumstances and all
                           data is backed up on a daily basic.
                           {/*<span className="color_sna">Learn more</span>*/}
                        </p>
                     </div>
                     <div className="col-12 col-lg-4 security_item text-center p-4">
                        <img src={icon3} alt="dedicated" className="my-5 animated delay-1"/>
                        <p className="font-RoL security_item_title color_sna mb-4">Physical Security</p>
                        <p className="font-RoR security_item_content text-white">
                           Physical security plays an essential part of security. With 24/7 security guard,
                           CCTV, UPS your project is protected.
                        </p>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default (Security);
