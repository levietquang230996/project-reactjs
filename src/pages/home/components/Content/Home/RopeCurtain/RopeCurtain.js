import React from 'react';

const RopeCurtain = (props) => {
   return (<div className="container d-flex justify-content-center mb-4 mb-md-5">
         <svg height="80" width="40">
            <path d="M20 0 L20 75 Z" stroke="#5c5c5c" strokeWidth="0.5" fill="none"/>
            <g stroke="#5c5c5c" strokeWidth="2" fill="#5c5c5c">
               <circle cx="20" cy="75" r="3"/>
            </g>
            Sorry, your browser does not support inline SVG.
         </svg>
      </div>
   )
};

export default RopeCurtain;
