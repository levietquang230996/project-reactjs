import React, {Component} from 'react';
import BGVideo from "./BGVideo/BGVideo";
import Section2Protfolio from "./Portfolio/Section2Protfolio";
import RopeCurtain from "./RopeCurtain/RopeCurtain";
import Offshore from "./Offshore/Offshore";
import HowWork from "./HowWork/HowWork";
import Sliders from "./Sliders/Sliders";

class ContentHome extends Component {

   render() {
      return (
         <React.Fragment>

            <BGVideo/>

            <RopeCurtain/>

            <Offshore/>

            <HowWork/>

            <RopeCurtain/>

            <Section2Protfolio/>

            <Sliders/>

         </React.Fragment>
      )
   }
}

export default ContentHome;
