import React, {Component} from 'react';
import img from '../../../../../../assests/images/home/offshore/offshore1.png';
import img1 from '../../../../../../assests/images/home/offshore/offshore2.png';
import img2 from '../../../../../../assests/images/home/offshore/offshore3.png';
import './index.css';
import $ from "jquery";

class Offshore extends Component {

   componentDidMount() {
      // const obj_image = $('.container-offshoreImg img')[0];
      // obj_image.height = obj_image.clientWidth * 2 / 3;

      $(window).on("scroll", function () {
         if (130 < $(this).scrollTop() && $(this).scrollTop() < 500)
         $('.offshoreImg .animated').addClass('fadeInUp');
      });
   }

   render() {
      return (
         <React.Fragment>
            <section className="offshoreImg">
               <div className="container text-center py-3 py-md-4">
                  <p className="font-RoL custom_title" style={{color: '#403e3d'}}>
                     Offshore Development <span className="color_sna">Center</span>
                  </p>
                  <div className="font-RoR custom_content" style={{color: '#777777'}}>
                     With SNA, your gola is already success. While we offer services as many other offshore <br/>
                     company do, SNA is specialized on dedicated team, security and reliability
                  </div>
               </div>
               <div className="container row py-3 py-md-4 m-auto">
                  <div className="col-12 col-md-4  animated p-3 delay-1">
                     <div className="container-offshoreImg position-relative w-100">
                        <img src={img} alt="alt" className="w-100"/>
                        <div className="containerBack-offshoreImg position-absolute">
                           <i className="fa fa-plus" aria-hidden="true"/>
                        </div>
                     </div>
                     <p className="custom_title_card mt-3 mt-md-4 mb-1">Decided Team</p>
                     <div className="custom_content">
                        Hypertext Markup Language (HTML) is the standard markup language for documents designed
                        to be
                        displayed in a web browser.
                     </div>
                  </div>
                  <div className="col-12 col-md-4 animated  p-3 delay-2">
                     <div className="container-offshoreImg position-relative w-100">
                        <img src={img1} alt="alt" className="w-100"/>
                        <div className="containerBack-offshoreImg position-absolute">
                           <i className="fa fa-plus" aria-hidden="true"/>
                        </div>
                     </div>
                     <p className="custom_title_card mt-3 mt-md-4 mb-1">Decided Team</p>
                     <div className="custom_content">
                        Hypertext Markup Language (HTML) is the standard markup language for documents designed
                        to be
                        displayed in a web browser.
                     </div>
                  </div>
                  <div className="col-12 col-md-4 animated  p-3 delay-3">
                     <div className="container-offshoreImg position-relative w-100">
                        <img src={img2} alt="alt" className="w-100"/>
                        <div className="containerBack-offshoreImg position-absolute">
                           <i className="fa fa-plus" aria-hidden="true"/>
                        </div>
                     </div>
                     <p className="custom_title_card mt-3 mt-md-4 mb-1">Decided Team</p>
                     <div className="custom_content">
                        Hypertext Markup Language (HTML) is the standard markup language for documents designed
                        to be
                        displayed in a web browser.
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default Offshore;
