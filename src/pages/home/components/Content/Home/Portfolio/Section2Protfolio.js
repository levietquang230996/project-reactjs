import React, {Component} from 'react';
import './index.css';
import $ from 'jquery';
import Shuffle from 'shufflejs/src/shuffle';

class Section2Protfolio extends Component {
   shuffle = null;
   state = {
      shuffle: null
   };

   componentDidMount() {
      // const type = Array.from(document.querySelectorAll('.controls span'));
      this.shuffle = new Shuffle(document.getElementsByClassName('my-shuffle-container')[0], {
         itemSelector: '.picture-item',
         sizer: '.my-sizer-element'
      });
   }

   render() {
      return (
         <React.Fragment>
            <section className="portfolio-container">
               <div className="text-center py-3">
                         <span className="custom_title font-RoL">
                         <span className="color_sna">OUR</span> PORTFOLIO</span>
               </div>
               <div className="d-flex flex-wrap justify-content-center controls m-auto text-dark pb-3 pb-md-5">
                  <span className="cursor-pointer py-0 px-3 active" data-group="all"
                        onClick={this.onChangeObject}>All</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="city"
                        onClick={this.onChangeObject}>MOBILE APPS</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="nature"
                        onClick={this.onChangeObject}>SMART FACTORY</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="space"
                        onClick={this.onChangeObject}>FINANCE</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="animal"
                        onClick={this.onChangeObject}>VIDEO</span>
                  <span className="cursor-pointer py-0 px-3 " data-group="space"
                        onClick={this.onChangeObject}>BUSINESS</span>
               </div>
               <div className="w-100">
                  <div id="grid" className="row my-shuffle-container">
                     <figure className="col-6@xs col-6@sm col-6@md picture-item picture-item--overlay"
                             data-groups='["city"]'
                             data-title="Golden Gate Bridge">
                        <div className="picture-item__inner">
                           <img src="http://regaltheme.com/tf/multi/bizli/assets/img/portfolio/w1.jpg"
                                alt="Ahihi"/>
                           <div className="folio-title">
                              <span>
                                 <h2 className="white-color">Awesome Mockup</h2>
                              </span>
                              <p className="white-color">Branding</p>
                              <hr/>
                           </div>
                        </div>
                     </figure>
                     <figure className="col-3@xs col-6@sm col-3@md picture-item" data-groups='["nature"]'
                             data-title="Lake">
                        <div className="picture-item__inner">
                           <img src="http://regaltheme.com/tf/multi/bizli/assets/img/portfolio/s1.jpg"
                                alt="Ahihi"/>
                           <div className="folio-title">
                              <span>
                                 <h2 className="white-color">Awesome Mockup</h2>
                              </span>
                              <p className="white-color">Branding</p>
                              <hr/>
                           </div>
                        </div>
                     </figure>
                     <figure className="col-3@xs col-6@sm col-3@md picture-item" data-groups='["animal"]'
                             data-title="Crocodile">
                        <div className="picture-item__inner">
                           <img src="http://regaltheme.com/tf/multi/bizli/assets/img/portfolio/s2.jpg"
                                alt="Ahihi"/>
                           <div className="folio-title">
                              <span>
                                 <h2 className="white-color">Awesome Mockup</h2>
                              </span>
                              <p className="white-color">Branding</p>
                              <hr/>
                           </div>
                        </div>
                     </figure>
                     <figure className="col-6@xs col-6@sm col-6@md picture-item picture-item--overlay"
                             data-groups='["space","nature"]'
                             data-title="Milky Way">
                        <div className="picture-item__inner">
                           <img src="http://regaltheme.com/tf/multi/bizli/assets/img/portfolio/w2.jpg"
                                alt="Ahihi"/>
                           <div className="folio-title">
                              <span>
                                 <h2 className="white-color">Awesome Mockup</h2>
                              </span>
                              <p className="white-color">Branding</p>
                              <hr/>
                           </div>
                        </div>
                     </figure>
                     <figure className="col-3@xs col-6@sm col-3@md picture-item"
                             data-groups='["space"]' data-title="SpaceX">
                        <div className="picture-item__inner">
                           <img src="http://regaltheme.com/tf/multi/bizli/assets/img/portfolio/s3.jpg"
                                alt="Ahihi"/>
                           <div className="folio-title">
                              <span>
                                 <h2 className="white-color">Awesome Mockup</h2>
                              </span>
                              <p className="white-color">Branding</p>
                              <hr/>
                           </div>
                        </div>
                     </figure>
                     <figure className="col-3@xs col-6@sm col-3@md picture-item" data-groups='["city"]'
                             data-title="Crossroads">
                        <div className="picture-item__inner">
                           <img src="http://regaltheme.com/tf/multi/bizli/assets/img/portfolio/s4.jpg"
                                alt="Ahihi"/>
                           <div className="folio-title ">
                              <span>
                                 <h2 className="white-color">Awesome Mockup</h2>
                              </span>
                              <p className="white-color">Branding</p>
                              <hr/>
                           </div>
                        </div>
                     </figure>
                     <div className="col-1@sm col-1@xs my-sizer-element"/>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }

   onChangeObject = (event) => {
      event.persist();
      const btn = event.target;
      $('.controls span').removeClass('active');
      btn.classList.add('active');
      const btnGroup = btn.getAttribute('data-group');
      if (btn.getAttribute('data-group').includes('all')) {
         this.shuffle.filter(Shuffle.ALL_ITEMS);
         return;
      }
      this.shuffle.filter(btnGroup);
   };
}

export default Section2Protfolio;
