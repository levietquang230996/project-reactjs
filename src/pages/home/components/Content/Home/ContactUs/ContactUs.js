import React, {Component} from 'react';
import './index.css';
import {ListContactUs, getRandomInt} from './RandomInList';

class ContactUs extends Component {

   state = {
      index: 0
   };

   componentDidMount() {
      this.setState({
         index: getRandomInt(ListContactUs.length)
      });
   }

   render() {
      return (
         <React.Fragment>
            <section className="contactUs-home w-100">
               <div className="container row m-auto">
                  <div className="w-80">
                     <p className="font-RoR contactUs-home-title text-uppercase text-white">
                        {ListContactUs[this.state.index].title}
                     </p>
                     <p className="font-RoL contactUs-home-content text-white">
                        {ListContactUs[this.state.index].content}
                     </p>
                  </div>
                  <div className="w-20 d-flex align-items-center justify-content-center">
                     <button className="font-RoM btn btn_contactUs text-uppercase text-white shadow">
                        contact us
                     </button>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }

}

export default ContactUs;
