import React, {Component} from 'react';
import $ from "jquery";

const img1 = 'https://eastagile-website.s3.amazonaws.com/production/uploads/ckeditor_assets/Images/content_twitter.png';
const img2 = 'https://eastagile-website.s3.amazonaws.com/production/uploads/ckeditor_assets/Images/content_thehomedepot.png';
const img3 = 'https://eastagile-website.s3.amazonaws.com/production/uploads/ckeditor_assets/Images/content_swink.png';

class Sliders extends Component {

   componentDidMount() {
      $(".autoplay").slick({
         slidesToShow: 3,
         slidesToScroll: 2,
         rows: 2,
         dots: true,
         arrows: false,
         // infinite: true,
         // autoplay: true,
         // autoplaySpeed: 0,
         // speed: 5000,
         cssEase: "linear",
         responsive: [
            {
               breakpoint: 1200,
               settings: {
                  slidesToShow: 3
               }
            },
            {
               breakpoint: 768,
               settings: {
                  slidesToShow: 2
               }
            },
            {
               breakpoint: 576,
               settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
               }
            }
         ]
      });
   }

   render() {
      return (
         <React.Fragment>
            <section id="content5">
               <div className="container row m-auto py-3 py-md-5">
                  <div className="col-12 col-lg-6 my-3 my-lg-0">
               <span className="custom_title font-RoL">
                   <span className="color_sna">OUR</span> Partner's</span>
                     <div className=" w-100 px-3 pt-4 pb-5 bg-slider">
                        <div className="autoplay">
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img1}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img2}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img3}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img2}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img1}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img3}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img1}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img3}/>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="col-12 col-lg-6 my-3 my-lg-0">
               <span className="custom_title font-RoL">
                   <span className="color_sna">OUR</span> Client's</span>
                     <div className=" w-100 px-3 pt-4 pb-5 bg-slider">
                        <div className="autoplay">
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img1}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img2}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img3}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img2}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img1}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img3}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img1}/>
                           </div>
                           <div>
                              <img alt="panel" className="rounded w-100"
                                   src={img3}/>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </React.Fragment>
      )
   }
}

export default Sliders;
