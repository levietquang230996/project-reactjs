import React, {Component} from 'react';
import ContentWhySna from "../components/Content/WhySna/ContentWhySna";
import Loading from "../components/Loading/Loading";

class PageWhySna extends Component {

   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 1000)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentWhySna/>

         </React.Fragment>
      )
   }
}

export default (PageWhySna);
