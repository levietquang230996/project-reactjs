import React, {Component} from 'react';
import Loading from "../components/Loading/Loading";
import ContentProcess from "../components/Content/Process/ContentProcess";

class PageProcess extends Component {
   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 1000)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentProcess/>

         </React.Fragment>
      )
   }
}

export default (PageProcess);
