import React, {Component} from 'react';
import ContentHome from "../components/Content/Home/Content-Home";
import Loading from "../components/Loading/Loading";

class PageHome extends Component {

   state = {loading: true};

   componentDidMount() {
      const self = this;
      setTimeout(function () {
         self.setState({loading: false});
      }, 1000)
   }

   render() {
      return (
         <React.Fragment>

            <Loading loading={this.state.loading}/>

            <ContentHome/>

         </React.Fragment>
      )
   }
}

export default (PageHome);
