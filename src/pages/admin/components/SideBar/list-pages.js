export const listPages = [
   {
      key: 'users',
      icon: 'user',
      title: 'Users'
   },
   {
      key: 'portfolio',
      icon: 'team',
      title: 'Portfolio'
   },
];
