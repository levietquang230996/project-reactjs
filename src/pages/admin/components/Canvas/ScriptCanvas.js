import Swal from 'sweetalert2'

let img, canvas, ctx, canvas_self, type_self;

function Annotation_Drawable_Canvas(canvas) {
   // canvas_self is main ( not change but type_self can change )
   canvas_self = this;
   this.boxes = [];
   this.coordinates = [];
   // -2 b/c border 1px canvas left and right
   this.width = canvas.getBoundingClientRect().width - 2;
   this.height = canvas.getBoundingClientRect().height - 2;

   // POLYGON


   this.init();
}

Annotation_Drawable_Canvas.prototype = {
   init: function () {
      const rect = JSON.parse(localStorage.getItem('rect'));
      const polygon = JSON.parse(localStorage.getItem('polygon'));
      (rect === null) ? this.boxes = [] : this.boxes = rect;
      (polygon === null) ? this.coordinates = [] : this.coordinates = polygon;
      // redrawRect rectangle drew
      for (let i = 0; i < this.boxes.length; i++) {
         if (this.boxes[i].type === 'rectangle') {
            this.drawRectangle(this.boxes[i], ctx, 0);
         }
      }
      // redrawRect rectangle drew
      for (let i = 0; i < this.coordinates.length; i++) {
         if (this.coordinates[i].type === 'polygon') {
            this.drawPolygon(this.coordinates[i]);
         }
      }
   },
   handleEvent: function (mouse, annotation_type, event) {
      if (annotation_type === 'rectangle') {
         type_self.handleDrawRect(mouse, event)
      } else if (annotation_type === 'polygon') {
         type_self.handleDrawPolygon(mouse, event)
      }
   },
   changeType: function (type) {
      if (type === 'rectangle') {
         // get properties of rectangle
         new Annotation_Drawable_Rectangle();
      } else if (type === 'polygon') {
         // get properties of rectangle
         new Annotation_Drawable_Polygon();
      }
   },

   // RECTANGLE
   // Get OBJECT and draw on canvas
   drawRectangle: function (box, context, dotSize) {
      const xCenter = box.rect_x1 + (box.rect_x2 - box.rect_x1) / 2;
      const yCenter = box.rect_y1 + (box.rect_y2 - box.rect_y1) / 2;

      context.fillStyle = box.color;
      context.strokeStyle = box.color;
      context.lineWidth = box.rect_lineWidth;
      // get position x, y, width, height of rectangle
      context.rect(box.rect_x1, box.rect_y1, (box.rect_x2 - box.rect_x1), (box.rect_y2 - box.rect_y1));
      // to draw rect ( connect all point )
      context.stroke();
      // fillRect : to draw rect ( DOTS )
      context.fillRect(box.rect_x1 - dotSize, box.rect_y1 - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(box.rect_x1 - dotSize, yCenter - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(box.rect_x1 - dotSize, box.rect_y2 - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(xCenter - dotSize, box.rect_y1 - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(xCenter - dotSize, yCenter - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(xCenter - dotSize, box.rect_y2 - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(box.rect_x2 - dotSize, box.rect_y1 - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(box.rect_x2 - dotSize, yCenter - dotSize, 2 * dotSize, 2 * dotSize);
      context.fillRect(box.rect_x2 - dotSize, box.rect_y2 - dotSize, 2 * dotSize, 2 * dotSize);
   },

   // Polygon
   //newPolygon =this.coordinates[index].dots
   drawPolygon: function (newPolygon) {
      newPolygon.dotSize=0;
      ctx.moveTo(newPolygon.dots[0].x, newPolygon.dots[0].y);
      ctx.beginPath();
      for (let i = 0; i < newPolygon.dots.length; i++) {

         // color dot
         ctx.fillStyle = newPolygon.fillColor;
         ctx.fillRect(
            newPolygon.dots[i].x - newPolygon.dotSize,
            newPolygon.dots[i].y - newPolygon.dotSize,
            2 * newPolygon.dotSize,
            2 * newPolygon.dotSize
         );

         // draw line
         ctx.strokeStyle = newPolygon.fillColor;
         ctx.lineTo(newPolygon.dots[i].x, newPolygon.dots[i].y);
         ctx.fillStyle = newPolygon.fillColor + '4a';
      }
      ctx.closePath();
      ctx.stroke();
      ctx.fill();
   }
};

// RECTANGLE
function Annotation_Drawable_Rectangle() {
   type_self = this;

   // RECTANGLE
   this.rect_x1 = -1;
   this.rect_y1 = -1;
   this.rect_x2 = -1;
   this.rect_y2 = -1;
   // side dot
   this.rect_dotSize = 2.5;
   // border color
   this.rect_Color = '#00b0ff';
   // position of dot to scaleRect rectangle
   this.rect_lineOffset = 4;
   // size border
   this.rect_lineWidth = 2;
   this.rect_clickedArea = {box: -1, pos: 'o'};
   // position of canvas
   this.canvas_x = canvas.getBoundingClientRect().x;
   this.canvas_y = canvas.getBoundingClientRect().y;
   this.check_moving = false;
   this.init();
}

Annotation_Drawable_Rectangle.prototype = {
   init: function () {
      // console.log(type_self, canvas_self)
   },
   // event mouse
   handleDrawRect: function (mouse, event) {
      if (mouse === 'down') {
         this.mouseDown(event);
      } else if (mouse === 'move') {
         this.mouseMove(event);
      } else if (mouse === 'up') {
         this.mouseUp();
      } else if (mouse === 'out' && this.rect_clickedArea) {
         this.mouseOut();
      }
   },
   mouseDown: function (event) {
      // get canvas_x , canvas_y when scroll page
      this.canvas_x = canvas.getBoundingClientRect().x;
      this.canvas_y = canvas.getBoundingClientRect().y;
      const currX = event.clientX - this.canvas_x;
      const currY = event.clientY - this.canvas_y;

      this.rect_x1 = this.rect_x2 = currX;
      this.rect_y1 = this.rect_y2 = currY;
      this.rect_clickedArea = this.findCurrentArea(currX, currY);

      // mouse down at rect to show dots
      if (this.rect_clickedArea.box === -1) {
         this.redrawRect();
      } else {
         this.rectSelect(this.rect_clickedArea);
      }

   },
   mouseMove: function (event) {
      this.rect_x2 = event.clientX - this.canvas_x;
      this.rect_y2 = event.clientY - this.canvas_y;
      // draw new rectangle
      if (this.rect_clickedArea.box === -1) {
         this.startDrawRect();
      } else { // redrawRect rectangle
         this.check_moving = true;
         this.scaleRect();
         this.rectSelect(this.rect_clickedArea);
      }
   },
   mouseUp: function () {
      if (this.tmpBox) {
         canvas_self.boxes.push(this.tmpBox);
         localStorage.setItem('rect', JSON.stringify(canvas_self.boxes));
         this.tmpBox = undefined;
      }
      if (this.check_moving) {
         this.movingRect();
      }
   },
   mouseOut: function () {
      if (this.rect_clickedArea.box !== -1) {
         var selectedBox = canvas_self.boxes[this.rect_clickedArea.box];
         if (selectedBox.rect_x1 > this.rect_x2) {
            selectedBox.rect_x1 = this.rect_x2;
         }
         if (selectedBox.rect_y1 > this.rect_y2) {
            selectedBox.rect_y1 = this.rect_y2;
         }
      }
      if (this.tmpBox) {
         canvas_self.boxes.push(this.tmpBox);
         localStorage.setItem('rect', JSON.stringify(canvas_self.boxes));
         this.tmpBox = undefined;
      }
   },

   // Includes: Redraw all annotation were drawn ,  newRect return OBJECT , drawRectangle get OBJECT and draw on canvas
   startDrawRect: function () {
      this.redrawRect();
      // RECTANGLE : draw rectangle when click on canvas without any rect
      if (this.rect_clickedArea.box === -1) {
         this.tmpBox = this.newRect(this.rect_x1, this.rect_y1, this.rect_x2, this.rect_y2);
         if (this.tmpBox != null) {
            canvas_self.drawRectangle(this.tmpBox, ctx, this.rect_dotSize);
         }
      }

   },
   // Redraw all annotation were drawn
   redrawRect: function () {
      ctx.clearRect(0, 0, canvas_self.width, canvas_self.height);
      ctx.drawImage(img, 0, 0);
      ctx.beginPath();

      // redrawRect rectangle drew
      for (let i = 0; i < canvas_self.boxes.length; i++) {
         canvas_self.drawRectangle(canvas_self.boxes[i], ctx, 0);
      }
   },
   // Return OBJECT
   newRect(rect_x1, rect_y1, rect_x2, rect_y2) {
      const boxrect_x1 = rect_x1 < rect_x2 ? rect_x1 : rect_x2;
      const boxrect_y1 = rect_y1 < rect_y2 ? rect_y1 : rect_y2;
      const boxrect_x2 = rect_x1 > rect_x2 ? rect_x1 : rect_x2;
      const boxrect_y2 = rect_y1 > rect_y2 ? rect_y1 : rect_y2;
      if (boxrect_x2 - boxrect_x1 > this.rect_lineOffset * 2 && boxrect_y2 - boxrect_y1 > this.rect_lineOffset * 2) {
         return {
            rect_x1: boxrect_x1,
            rect_y1: boxrect_y1,
            rect_x2: boxrect_x2,
            rect_y2: boxrect_y2,
            rect_lineWidth: this.rect_lineWidth,
            color: this.rect_Color,
            type: 'rectangle'
         };
      } else {
         return null;
      }
   },
   // show dots when select rect
   rectSelect: function (Area) {
      ctx.clearRect(0, 0, canvas_self.width, canvas_self.height);
      ctx.drawImage(img, 0, 0);
      ctx.beginPath();

      // redrawRect rectangle drew
      for (let i = 0; i < canvas_self.boxes.length; i++) {
         if (Area.box === i) {
            canvas_self.drawRectangle(canvas_self.boxes[i], ctx, this.rect_dotSize);
         } else {
            canvas_self.drawRectangle(canvas_self.boxes[i], ctx, 0);

         }
      }
   },
   // check click in rectangle
   findCurrentArea: function (x, y) {
      for (let i = 0; i < canvas_self.boxes.length; i++) {
         const box = canvas_self.boxes[i];
         const xCenter = box.rect_x1 + (box.rect_x2 - box.rect_x1) / 2;
         const yCenter = box.rect_y1 + (box.rect_y2 - box.rect_y1) / 2;
         if (box.rect_x1 - this.rect_lineOffset < x && x < box.rect_x1 + this.rect_lineOffset) {
            if (box.rect_y1 - this.rect_lineOffset < y && y < box.rect_y1 + this.rect_lineOffset) {
               return {box: i, pos: 'tl'};
            } else if (box.rect_y2 - this.rect_lineOffset < y && y < box.rect_y2 + this.rect_lineOffset) {
               return {box: i, pos: 'bl'};
            } else if (yCenter - this.rect_lineOffset < y && y < yCenter + this.rect_lineOffset) {
               return {box: i, pos: 'l'};
            }
         } else if (box.rect_x2 - this.rect_lineOffset < x && x < box.rect_x2 + this.rect_lineOffset) {
            if (box.rect_y1 - this.rect_lineOffset < y && y < box.rect_y1 + this.rect_lineOffset) {
               return {box: i, pos: 'tr'};
            } else if (box.rect_y2 - this.rect_lineOffset < y && y < box.rect_y2 + this.rect_lineOffset) {
               return {box: i, pos: 'br'};
            } else if (yCenter - this.rect_lineOffset < y && y < yCenter + this.rect_lineOffset) {
               return {box: i, pos: 'r'};
            }
         } else if (xCenter - this.rect_lineOffset < x && x < xCenter + this.rect_lineOffset) {
            if (box.rect_y1 - this.rect_lineOffset < y && y < box.rect_y1 + this.rect_lineOffset) {
               return {box: i, pos: 't'};
            } else if (box.rect_y2 - this.rect_lineOffset < y && y < box.rect_y2 + this.rect_lineOffset) {
               return {box: i, pos: 'b'};
            } else if (box.rect_y1 - this.rect_lineOffset < y && y < box.rect_y2 + this.rect_lineOffset) {
               return {box: i, pos: 'i'};
            }
         } else if (box.rect_x1 - this.rect_lineOffset < x && x < box.rect_x2 + this.rect_lineOffset) {
            if (box.rect_y1 - this.rect_lineOffset < y && y < box.rect_y2 + this.rect_lineOffset) {
               return {box: i, pos: 'i'};
            }
         }
      }
      return {box: -1, pos: 'o'};
   },
   // delete rectangle
   deleteRect: function (event) {
      if (this.rect_clickedArea) {

         Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
         }).then((result) => {
            if (result.value) {
               canvas_self.boxes.splice(this.rect_clickedArea.box, 1);
               this.rect_clickedArea = undefined;
               localStorage.setItem('rect', JSON.stringify(canvas_self.boxes));
               this.redrawRect(this.rect_clickedArea);
               // Swal.fire(
               //    'Deleted!',
               //    'Your file has been deleted.',
               //    'success'
               // )
            }
         })
      }
   },
   // Resize rectangle
   scaleRect: function () {
      const xOffset = this.rect_x2 - this.rect_x1;
      const yOffset = this.rect_y2 - this.rect_y1;
      this.rect_x1 = this.rect_x2;
      this.rect_y1 = this.rect_y2;

      if (this.rect_clickedArea.pos === 'i' || this.rect_clickedArea.pos === 'tl' ||
         this.rect_clickedArea.pos === 'l' || this.rect_clickedArea.pos === 'bl') {
         canvas_self.boxes[this.rect_clickedArea.box].rect_x1 += xOffset;
      }
      if (this.rect_clickedArea.pos === 'i' || this.rect_clickedArea.pos === 'tl' ||
         this.rect_clickedArea.pos === 't' || this.rect_clickedArea.pos === 'tr') {
         canvas_self.boxes[this.rect_clickedArea.box].rect_y1 += yOffset;
      }
      if (this.rect_clickedArea.pos === 'i' || this.rect_clickedArea.pos === 'tr' ||
         this.rect_clickedArea.pos === 'r' || this.rect_clickedArea.pos === 'br') {
         canvas_self.boxes[this.rect_clickedArea.box].rect_x2 += xOffset;
      }
      if (this.rect_clickedArea.pos === 'i' || this.rect_clickedArea.pos === 'bl' ||
         this.rect_clickedArea.pos === 'b' || this.rect_clickedArea.pos === 'br') {
         canvas_self.boxes[this.rect_clickedArea.box].rect_y2 += yOffset;
      }
   },
   // Moving rectangle
   movingRect: function () {
      let minX, maxX, minY, maxY;
      this.check_moving = false;
      canvas_self.boxes.map((box, index) => {
         if (this.rect_clickedArea.box === index) {
            if (box.rect_x1 < box.rect_x2) {
               minX = box.rect_x1;
               maxX = box.rect_x2;
            } else {
               minX = box.rect_x2;
               maxX = box.rect_x1;
            }
            if (box.rect_y1 < box.rect_y2) {
               minY = box.rect_y1;
               maxY = box.rect_y2;
            } else {
               minY = box.rect_y2;
               maxY = box.rect_y1;
            }

            this.tmpBox = this.newRect(minX, minY, maxX, maxY);
            if (this.tmpBox !== null) {
               canvas_self.drawRectangle(this.tmpBox, ctx, this.rect_dotSize);
               canvas_self.boxes[index].rect_x1 = minX;
               canvas_self.boxes[index].rect_x2 = maxX;
               canvas_self.boxes[index].rect_y1 = minY;
               canvas_self.boxes[index].rect_y2 = maxY;
               localStorage.setItem('rect', JSON.stringify(canvas_self.boxes));
               this.tmpBox = undefined;
            }
         }
         return true;
      });
   }
};

// POLYGON
function Annotation_Drawable_Polygon() {
   type_self = this;
   this.isDone = true;
   this.polygon_dotSize = 3;
   this.polygon_fillColor = '#ff0000';
   this.canvas_x = canvas.getBoundingClientRect().x;
   this.canvas_y = canvas.getBoundingClientRect().y;
   this.init();
}

Annotation_Drawable_Polygon.prototype = {
   init: function () {

   },
   handleDrawPolygon: function (mouse, event) {
      if (mouse === 'down') {
         this.mouseDown(event);
      }
   },
   mouseDown: function (event) {
      this.canvas_x = canvas.getBoundingClientRect().x;
      this.canvas_y = canvas.getBoundingClientRect().y;
      const currX = event.clientX - this.canvas_x;
      const currY = event.clientY - this.canvas_y;
      let lastItem = canvas_self.coordinates.length - 1;

      if (this.isDone) {
         // create dot[0]
         this.isDone = false;
         canvas_self.coordinates.push({
            type: 'polygon',
            dotSize: this.polygon_dotSize,
            fillColor: this.polygon_fillColor,
            dots: [{x: currX, y: currY}]
         })
         lastItem = canvas_self.coordinates.length - 1
      } else {
         // check click at dot[0] or not
         const check = this.checkFinishDraw(currX, currY);
         if (check) {
            this.isDone = true;
            localStorage.setItem('polygon', JSON.stringify(canvas_self.coordinates));
            this.polygon_fillColor = "#ffffff";
         } else {
            // create new dot[n]
            canvas_self.coordinates[lastItem].dots.push({
               x: currX, y: currY
            })
         }
      }
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      ctx.drawImage(img, 0, 0);

      for (let i = 0; i < canvas_self.coordinates.length; i++) {
         canvas_self.drawPolygon(canvas_self.coordinates[i]);
      }

      const newPolygon = canvas_self.coordinates[lastItem];
      canvas_self.drawPolygon(newPolygon);

   },
   // kiem tra current co trong vi tri 0 hay chwa neu chwa ve tiep ?
   checkFinishDraw: function (currX, currY) {
      const lastItem = canvas_self.coordinates.length - 1;
      if (lastItem < 0) {
         this.isDone = true;
         return false;
      }

      const firstDotX = canvas_self.coordinates[lastItem].dots[0].x;
      const firstDotY = canvas_self.coordinates[lastItem].dots[0].y;
      // click at dot[0] => DONE
      if (firstDotX - this.polygon_dotSize < currX && currX < firstDotX + this.polygon_dotSize
         && firstDotY - this.polygon_dotSize < currY && currY < firstDotY + this.polygon_dotSize) {
         canvas_self.coordinates[lastItem].dots.push({
            x: firstDotX, y: firstDotY
         });
         return true;
      } else {
         return false;
      }
   }
};

// convert image to canvas 
export const get_background_canvas = () => {
   img = document.createElement('img');
   img.src = 'https://images.pexels.com/photos/373912/pexels-photo-373912.jpeg?auto=compress&cs=tinysrgb&h=650&w=940';
   img.width = 600;
   img.height = 400;
};

// get type annotation in canvas
export const init_global_canvas = (type) => {
   canvas = document.getElementById('canvas_annotaion');
   ctx = canvas.getContext('2d');

   // load finish img then set type for canvas
   img.onload = function () {
      ctx.drawImage(img, 0, 0);

      // get canvas default
      new Annotation_Drawable_Canvas(canvas);
      if (type === 'rectangle') {
         // get properties of rectangle
         new Annotation_Drawable_Rectangle();
      } else if (type === 'polygon') {
         // get properties of rectangle
         new Annotation_Drawable_Polygon();
      }
   }

};

// get event change type annotation ( Rectangle, Polygon )
export const handleChangeType = (type) => {
   canvas_self.changeType(type);
};

// key down in canvas
export const handle_keyDown_canvas = () => {
   document.addEventListener('keydown', function (event) {
      if (event.code === 'Delete') {
         type_self.deleteRect(event);
      }
   }, false);
};

export const handleOnMouse = (type, annotation_type, event) => {
   canvas_self.handleEvent(type, annotation_type, event);
};
