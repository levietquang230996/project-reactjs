import React, {Component} from 'react';
import Menu from "../components/Menu/Menu";
import SideBar from "../components/SideBar/SideBar";
import {Switch, Route, withRouter} from "react-router-dom";
import {list_router_pages} from "../../../router";


class PageAdmin extends Component {

   render() {
      return (
         <React.Fragment>
            {/*Header*/}
            <Menu/>

            {/*SideBar*/}
            <SideBar/>

            {/*Content*/}
            <div className="content-wrapper">
               <div className="container-fluid">
                  <div className="row">
                     <Switch>
                        {this.GetRouter(list_router_pages)}
                     </Switch>
                  </div>
               </div>
            </div>

         </React.Fragment>
      )
   }

   GetRouter = (routers) => (
      routers.map((routes) => (
         (routes.routes && routes.path === '/admin') ?
            routes.routes.map((router, i) => (
               <Route key={i} path={`${routes.path + router.path}`} component={router.main} exact={router.exact}/>
            )) : null
      ))
   )
}

export default withRouter(PageAdmin);
