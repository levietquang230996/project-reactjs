export const decrementAction = () => ({type: 'SEND_DECREMENT_NUMBER'});

export const incrementAction = () => ({type: 'SEND_INCREMENT_NUMBER'});