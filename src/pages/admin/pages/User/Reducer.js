import {
   SEND_DECREMENT_NUMBER,
   SEND_INCREMENT_NUMBER,
   SUCCESS_DECREMENT_NUMBER,
   SUCCESS_INCREMENT_NUMBER
} from "../../../../reducer/actions";

const initialState = {
   loading: false,
   num: 'zero'
};

export const changeNumber = (state = initialState, action) => {
   switch (action.type) {
      case SEND_DECREMENT_NUMBER:
         return {...state, loading: true};
      case SEND_INCREMENT_NUMBER:
         return {...state, loading: true};
      case SUCCESS_DECREMENT_NUMBER:
         return {...state, loading: false, num: action.num};
      case SUCCESS_INCREMENT_NUMBER:
         return {...state, loading: false, num: action.num};
      default:
         return state;
   }
}